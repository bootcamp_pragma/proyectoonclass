package com.onClass.proyectoonclass.adapters.driving.http.mapper;

import com.onClass.proyectoonclass.adapters.driving.http.dto.response.CapacityResponse;
import com.onClass.proyectoonclass.domain.model.Capacity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ICapacityResponseMapper {
    @Mapping(source = "idCapacity", target ="idCapacity")
    @Mapping(source = "name", target="name")
    @Mapping(source = "description", target="description")
    @Mapping(source = "technologies", target="technologies")
    CapacityResponse toCapacityResponse(Capacity capacity);

    List<CapacityResponse> toCapacityResponseList(List<Capacity> capacities);
}
