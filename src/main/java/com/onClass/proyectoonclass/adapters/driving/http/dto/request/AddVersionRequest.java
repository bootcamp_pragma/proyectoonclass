package com.onClass.proyectoonclass.adapters.driving.http.dto.request;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;
@AllArgsConstructor
@Getter
public class AddVersionRequest {

    private final Long idVersion;

    @NotNull
    private final Integer cupo;

    @NotNull
    private final Date fechaInicio;

    @NotNull
    private final Date fechaFin;

    @NotNull
    private final Long idBootcamp;

}
