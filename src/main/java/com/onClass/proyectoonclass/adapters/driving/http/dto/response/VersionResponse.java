package com.onClass.proyectoonclass.adapters.driving.http.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;
@AllArgsConstructor
@Getter
public class VersionResponse {

    private final Long idVersion;

    private final Integer cupo;

    private final Date fechaInicio;

    private final Date fechaFin;

    private final Long idBootcamp;

    private final String bootcamp;

}
