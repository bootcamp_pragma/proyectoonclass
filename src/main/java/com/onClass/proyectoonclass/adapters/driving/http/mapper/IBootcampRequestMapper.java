package com.onClass.proyectoonclass.adapters.driving.http.mapper;

import com.onClass.proyectoonclass.adapters.driving.http.dto.request.AddBootcampRequest;
import com.onClass.proyectoonclass.adapters.driving.http.dto.request.UpdateBootcampRequest;
import com.onClass.proyectoonclass.domain.model.Bootcamp;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "Spring")
public interface IBootcampRequestMapper {
    @Mapping(source = "idBootcamp", target="idBootcamp")
    @Mapping(source = "name", target="name")
    @Mapping(source = "description", target="description")
    @Mapping(source = "capacities", target = "capacities")
    Bootcamp addRequestToBootcamp(AddBootcampRequest bootcampRequest);
    @Mapping(source = "idBootcamp", target="idBootcamp")
    @Mapping(source = "name", target="name")
    @Mapping(source = "description", target="description")
    @Mapping(source = "capacities", target = "capacities")
    Bootcamp updateRequestToBootcamp(UpdateBootcampRequest bootcampRequest);
}
