package com.onClass.proyectoonclass.adapters.driving.http.controller;

import com.onClass.proyectoonclass.adapters.driving.http.dto.request.AddTechnologyRequest;
import com.onClass.proyectoonclass.adapters.driving.http.dto.request.UpdateTechnologyRequest;
import com.onClass.proyectoonclass.adapters.driving.http.dto.response.TechnologyResponse;
import com.onClass.proyectoonclass.adapters.driving.http.mapper.ITechnologyRequestMapper;
import com.onClass.proyectoonclass.adapters.driving.http.mapper.ITechnologyResponseMapper;
import com.onClass.proyectoonclass.domain.api.ITechnologyServicePort;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/technology")
@RequiredArgsConstructor
public class TechnologyRestControllerAdapter {

    private final ITechnologyServicePort technologyServicePort;
    private final ITechnologyRequestMapper technologyRequestMapper;
    private final ITechnologyResponseMapper technologyResponseMapper;


    @PostMapping("/")
    public ResponseEntity<TechnologyResponse> addTechnology(@Valid @RequestBody AddTechnologyRequest request) {
        return ResponseEntity.status(HttpStatus.CREATED).body(technologyResponseMapper.toTechnologyResponse(
                technologyServicePort.saveTechnology(technologyRequestMapper.addRequestToTechnology(request))));
    }

    @GetMapping("/search/{technologyName}")
    public ResponseEntity<TechnologyResponse> getTechnology(@PathVariable String technologyName) {
        return ResponseEntity.ok(technologyResponseMapper.toTechnologyResponse(technologyServicePort.getTechnology(technologyName).get()));
    }

    @GetMapping("/")
    public ResponseEntity<List<TechnologyResponse>> getAllTechnology(@RequestParam Integer page, @RequestParam Integer size, @RequestParam String sort) {
        return ResponseEntity.ok(technologyResponseMapper.toTechnologyResponseList(technologyServicePort.getAllTechnology(page, size, sort)));
    }

    @PutMapping("/")
    public ResponseEntity<TechnologyResponse> updateTechnology(@RequestBody UpdateTechnologyRequest request) {
        return ResponseEntity.ok(technologyResponseMapper.toTechnologyResponse(
                technologyServicePort.updateTechnology(technologyRequestMapper.updateRequestToTechnology(request))
        ));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Map<String, Object>> deleteTechnology(@PathVariable Long id) {
        technologyServicePort.deleteTechnology(id);
        Map<String, Object> response = new HashMap<>();
        response.put("message", "Element " + id + " deleted successfully");
        return ResponseEntity.ok(response);
    }

}
