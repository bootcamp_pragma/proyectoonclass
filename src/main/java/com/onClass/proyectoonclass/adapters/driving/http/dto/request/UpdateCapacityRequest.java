package com.onClass.proyectoonclass.adapters.driving.http.dto.request;

import com.onClass.proyectoonclass.domain.model.Technology;
import jakarta.persistence.Column;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor
@Getter
public class UpdateCapacityRequest {
    private final Long idCapacity;
    @NotNull
    private final String name;

    @NotNull
    private final String description;

    private final List<Technology> technologies;
}
