package com.onClass.proyectoonclass.adapters.driving.http.dto.response;

import com.onClass.proyectoonclass.domain.model.Capacity;
import com.onClass.proyectoonclass.domain.model.Version;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor
@Getter
public class BootcampResponse {

    private final Long idBootcamp;
    private final String name;
    private final String description;
    private final List<Capacity> capacities;
    private final List<Version> versions;
}
