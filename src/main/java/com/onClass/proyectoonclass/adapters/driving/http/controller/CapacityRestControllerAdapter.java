package com.onClass.proyectoonclass.adapters.driving.http.controller;

import com.onClass.proyectoonclass.adapters.driving.http.dto.request.AddCapacityRequest;
import com.onClass.proyectoonclass.adapters.driving.http.dto.request.UpdateCapacityRequest;
import com.onClass.proyectoonclass.adapters.driving.http.dto.response.CapacityResponse;
import com.onClass.proyectoonclass.adapters.driving.http.mapper.ICapacityRequestMapper;
import com.onClass.proyectoonclass.adapters.driving.http.mapper.ICapacityResponseMapper;
import com.onClass.proyectoonclass.domain.api.ICapacityServicePort;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/capacity")
@RequiredArgsConstructor
public class CapacityRestControllerAdapter {

    private final ICapacityServicePort capacityServicePort;
    private final ICapacityRequestMapper capacityRequestMapper;
    private final ICapacityResponseMapper capacityResponseMapper;

    @PostMapping("/")
    public ResponseEntity<CapacityResponse> addCapacity(@Valid @RequestBody AddCapacityRequest request) {
        return ResponseEntity.status(HttpStatus.CREATED).body(capacityResponseMapper.toCapacityResponse(
                capacityServicePort.saveCapacity(capacityRequestMapper.addRequestToCapacity(request))));
    }

    @GetMapping("/search/{capacityName}")
    public ResponseEntity<CapacityResponse>getCapacity (@PathVariable String capacityName){
        return ResponseEntity.ok(capacityResponseMapper.toCapacityResponse(capacityServicePort.getCapacity(capacityName).get()));
    }

    @GetMapping("/")
    public ResponseEntity<List<CapacityResponse>> getAllCapacity(@RequestParam Integer page, @RequestParam Integer size, @RequestParam String sort) {
        return ResponseEntity.ok(capacityResponseMapper.toCapacityResponseList(capacityServicePort.getAllCapacity(page, size, sort)));
    }

    @PutMapping("/")
    public ResponseEntity<CapacityResponse> updateCapacity(@RequestBody UpdateCapacityRequest request){
        return ResponseEntity.ok(capacityResponseMapper.toCapacityResponse(
                capacityServicePort.updateCapacity(capacityRequestMapper.updateRequestToCapacity(request))
        ));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Map<String, Object>> deleteCapacity(@PathVariable Long id){
        capacityServicePort.deleteCapacity(id);
        Map<String, Object> response = new HashMap<>();
        response.put("message", "Element " + id + " deleted successfully");
        return ResponseEntity.ok(response);
    }

}
