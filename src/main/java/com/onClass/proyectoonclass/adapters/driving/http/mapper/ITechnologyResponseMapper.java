package com.onClass.proyectoonclass.adapters.driving.http.mapper;

import com.onClass.proyectoonclass.adapters.driving.http.dto.response.TechnologyResponse;
import com.onClass.proyectoonclass.domain.model.Technology;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;
@Mapper(componentModel = "spring")
public interface ITechnologyResponseMapper {
    @Mapping(source = "idTechnology", target ="idTechnology")
    @Mapping(source = "name", target="name")
    @Mapping(source = "description", target = "description")
    TechnologyResponse toTechnologyResponse(Technology technology);
    List<TechnologyResponse>toTechnologyResponseList(List<Technology> technologies);
}
