package com.onClass.proyectoonclass.adapters.driving.http.mapper;

import com.onClass.proyectoonclass.adapters.driving.http.dto.request.AddTechnologyRequest;
import com.onClass.proyectoonclass.adapters.driving.http.dto.request.UpdateTechnologyRequest;
import com.onClass.proyectoonclass.domain.model.Technology;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ITechnologyRequestMapper {

    @Mapping(source = "idTechnology", target ="idTechnology")
    @Mapping(source = "name", target="name")
    @Mapping(source = "description", target = "description")
    Technology addRequestToTechnology(AddTechnologyRequest addTechnologyRequest);

    @Mapping(source = "idTechnology", target ="idTechnology")
    @Mapping(source = "name", target="name")
    @Mapping(source = "description", target = "description")
    Technology updateRequestToTechnology(UpdateTechnologyRequest updateTechnologyRequest);
}
