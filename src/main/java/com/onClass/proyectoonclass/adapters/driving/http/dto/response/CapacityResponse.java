package com.onClass.proyectoonclass.adapters.driving.http.dto.response;

import com.onClass.proyectoonclass.domain.model.Technology;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor
@Getter
public class CapacityResponse {

    private final Long idCapacity;
    private final String name;
    private final String description;
    private final List<Technology> technologies;

}
