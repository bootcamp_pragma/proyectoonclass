package com.onClass.proyectoonclass.adapters.driving.http.mapper;

import com.onClass.proyectoonclass.adapters.driving.http.dto.request.AddVersionRequest;
import com.onClass.proyectoonclass.adapters.driving.http.dto.request.UpdateBootcampRequest;
import com.onClass.proyectoonclass.adapters.driving.http.dto.request.UpdateVersionRequest;
import com.onClass.proyectoonclass.adapters.driving.http.dto.response.VersionResponse;
import com.onClass.proyectoonclass.domain.model.Version;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface IVersionResponseMapper {

    @Mapping(source = "idVersion", target="idVersion")
    @Mapping(source = "cupo", target="cupo")
    @Mapping(source = "fechaInicio", target="fechaInicio")
    @Mapping(source = "fechaFin", target="fechaFin")
    @Mapping(source = "bootcamp.idBootcamp", target="idBootcamp")
    @Mapping(source = "bootcamp.name", target="bootcamp")
    VersionResponse toVersionResponse(Version version);

    List<VersionResponse> toVersionResponseList (List<Version> versions);
}
