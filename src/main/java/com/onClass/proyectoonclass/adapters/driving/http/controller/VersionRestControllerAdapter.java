package com.onClass.proyectoonclass.adapters.driving.http.controller;


import com.onClass.proyectoonclass.adapters.driving.http.dto.request.AddVersionRequest;
import com.onClass.proyectoonclass.adapters.driving.http.dto.request.UpdateBootcampRequest;
import com.onClass.proyectoonclass.adapters.driving.http.dto.request.UpdateVersionRequest;
import com.onClass.proyectoonclass.adapters.driving.http.dto.response.BootcampResponse;
import com.onClass.proyectoonclass.adapters.driving.http.dto.response.VersionResponse;
import com.onClass.proyectoonclass.adapters.driving.http.mapper.IVersionRequestMapper;
import com.onClass.proyectoonclass.adapters.driving.http.mapper.IVersionResponseMapper;
import com.onClass.proyectoonclass.domain.api.IVersionServicePort;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/version")
@RequiredArgsConstructor
public class VersionRestControllerAdapter {

    private final IVersionServicePort versionServicePort;
    private final IVersionRequestMapper versionRequestMapper;
    private final IVersionResponseMapper versionResponseMapper;


    @PostMapping("/")
    public ResponseEntity<VersionResponse> addVersion(@Valid @RequestBody AddVersionRequest request) {
        return ResponseEntity.status(HttpStatus.CREATED).body(versionResponseMapper.toVersionResponse(
                versionServicePort.saveVersion(versionRequestMapper.addRequestToVersion(request))));
    }

    @GetMapping("/")
    public ResponseEntity<List<VersionResponse>> getAllVersion(@RequestParam Integer page, @RequestParam Integer size, @RequestParam String sort) {
        return ResponseEntity.ok(versionResponseMapper.toVersionResponseList(versionServicePort.getAllVersion(page, size, sort)));
    }

    @GetMapping("/search/{idBootcamp}")
    public ResponseEntity<List<VersionResponse>> getAllVersionByBootcamp(@RequestParam Integer page,@RequestParam Integer size,@RequestParam String sort,@PathVariable Long idBootcamp) {
        return ResponseEntity.ok(versionResponseMapper.toVersionResponseList(versionServicePort.getAllVersionByBootcamp(idBootcamp, page, size, sort)));
    }

    @PutMapping("/")
    public ResponseEntity<VersionResponse> updateVersion(@RequestBody UpdateVersionRequest request) {
        return ResponseEntity.ok(versionResponseMapper.toVersionResponse(
                versionServicePort.updateVersion(versionRequestMapper.updateRequestToVersion(request))
        ));
    }
    @DeleteMapping("/{idVersion}")
    public ResponseEntity<Map<String, Object>> deleteVersion(@PathVariable Long idVersion) {
        versionServicePort.deleteVersion(idVersion);
        Map<String, Object> response = new HashMap<>();
        response.put("message", "Element " + idVersion + " deleted successfully");
        return ResponseEntity.ok(response);
    }
}
