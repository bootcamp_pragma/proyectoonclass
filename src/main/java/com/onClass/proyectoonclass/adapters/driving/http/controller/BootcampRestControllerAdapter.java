package com.onClass.proyectoonclass.adapters.driving.http.controller;

import com.onClass.proyectoonclass.adapters.driving.http.dto.request.AddBootcampRequest;
import com.onClass.proyectoonclass.adapters.driving.http.dto.request.UpdateBootcampRequest;
import com.onClass.proyectoonclass.adapters.driving.http.dto.response.BootcampResponse;
import com.onClass.proyectoonclass.adapters.driving.http.mapper.IBootcampRequestMapper;
import com.onClass.proyectoonclass.adapters.driving.http.mapper.IBootcampResponseMapper;
import com.onClass.proyectoonclass.domain.api.IBootcampServicePort;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/bootcamp")
@RequiredArgsConstructor
public class BootcampRestControllerAdapter {

        private final IBootcampServicePort bootcampServicePort;
        private final IBootcampRequestMapper bootcampRequestMapper;
        private final IBootcampResponseMapper bootcampResponseMapper;

        @PostMapping("/")
        public ResponseEntity<BootcampResponse> addBootcamp(@Valid @RequestBody AddBootcampRequest request){
            return ResponseEntity.status(HttpStatus.CREATED).body(bootcampResponseMapper.toBootcampResponse(
                    bootcampServicePort.saveBootcamp(bootcampRequestMapper.addRequestToBootcamp(request))));
        }

        @GetMapping("/search/{bootcampName}")
        public ResponseEntity<BootcampResponse>getBootcamp (@PathVariable String bootcampName) {
            return ResponseEntity.ok(bootcampResponseMapper.toBootcampResponse(bootcampServicePort.getBootcamp(bootcampName).get()));
        }

        @GetMapping("/")
        public ResponseEntity<List<BootcampResponse>> getAllBootcamp(@RequestParam Integer page, @RequestParam Integer size, @RequestParam String sort) {
            return ResponseEntity.ok(bootcampResponseMapper.toBootcampResponseList(bootcampServicePort.getAllBootcamp(page, size, sort)));
        }

        @PutMapping("/")
        public ResponseEntity<BootcampResponse> updateBootcamp(@RequestBody UpdateBootcampRequest request) {
            return ResponseEntity.ok(bootcampResponseMapper.toBootcampResponse(
                    bootcampServicePort.updateBootcamp(bootcampRequestMapper.updateRequestToBootcamp(request))
            ));
        }

        @DeleteMapping("/{id}")
        public ResponseEntity<Map<String, Object>> deleteBootcamp(@PathVariable Long id) {
            bootcampServicePort.deleteBootcamp(id);
            Map<String, Object> response = new HashMap<>();
            response.put("message", "Element " + id + " deleted successfully");
            return ResponseEntity.ok(response);
        }
}
