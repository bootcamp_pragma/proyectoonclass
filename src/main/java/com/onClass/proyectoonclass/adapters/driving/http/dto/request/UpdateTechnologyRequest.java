package com.onClass.proyectoonclass.adapters.driving.http.dto.request;

import jakarta.persistence.Column;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class UpdateTechnologyRequest {
    private final Long idTechnology;

    @Size(max = 50, message = "Nombre no debe exceder los 50 caracteres")
    private final String name;

    @Size(max = 90, message = "La descripción no debe superar los 90 caracteres")
    private final String description;
}
