package com.onClass.proyectoonclass.adapters.driving.http.mapper;

import com.onClass.proyectoonclass.adapters.driving.http.dto.request.AddVersionRequest;
import com.onClass.proyectoonclass.adapters.driving.http.dto.request.UpdateBootcampRequest;
import com.onClass.proyectoonclass.adapters.driving.http.dto.request.UpdateVersionRequest;
import com.onClass.proyectoonclass.domain.model.Version;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "Spring")
public interface IVersionRequestMapper {
    @Mapping(source = "idVersion", target="idVersion")
    @Mapping(source = "cupo", target="cupo")
    @Mapping(source = "fechaInicio", target="fechaInicio")
    @Mapping(source = "fechaFin", target="fechaFin")
    @Mapping(source = "idBootcamp", target="bootcamp.idBootcamp")
    @Mapping(target = "bootcamp.name", constant="name")
    @Mapping(target = "bootcamp.description", constant="description")
    Version addRequestToVersion(AddVersionRequest addVersionRequest);

    @Mapping(source = "idVersion", target="idVersion")
    @Mapping(source = "cupo", target="cupo")
    @Mapping(source = "fechaInicio", target="fechaInicio")
    @Mapping(source = "fechaFin", target="fechaFin")
    @Mapping(source = "idBootcamp", target="bootcamp.idBootcamp")
    @Mapping(target = "bootcamp.name", constant="name")
    @Mapping(target = "bootcamp.description", constant="description")
    Version updateRequestToVersion(UpdateVersionRequest updateBootcampRequest);
}
