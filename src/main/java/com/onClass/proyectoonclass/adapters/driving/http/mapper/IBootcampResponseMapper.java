package com.onClass.proyectoonclass.adapters.driving.http.mapper;

import com.onClass.proyectoonclass.adapters.driving.http.dto.response.BootcampResponse;
import com.onClass.proyectoonclass.domain.model.Bootcamp;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "Spring")
public interface IBootcampResponseMapper {
    @Mapping(source = "idBootcamp", target="idBootcamp")
    @Mapping(source = "name", target = "name")
    @Mapping(source = "description", target = "description")
    @Mapping(source = "capacities", target = "capacities")
    BootcampResponse toBootcampResponse(Bootcamp bootcamp);

    List<BootcampResponse> toBootcampResponseList (List<Bootcamp> bootcamps);
}
