package com.onClass.proyectoonclass.adapters.driving.http.mapper;

import com.onClass.proyectoonclass.adapters.driving.http.dto.request.AddCapacityRequest;
import com.onClass.proyectoonclass.adapters.driving.http.dto.request.UpdateCapacityRequest;
import com.onClass.proyectoonclass.domain.model.Capacity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "Spring")
public interface ICapacityRequestMapper {
    @Mapping(source = "idCapacity", target="idCapacity")
    @Mapping(source = "name", target = "name")
    @Mapping(source = "description", target = "description")
    @Mapping(source = "technologies", target="technologies")
    Capacity addRequestToCapacity(AddCapacityRequest capacityRequest);
    @Mapping(source = "idCapacity", target="idCapacity")
    @Mapping(source = "name", target = "name")
    @Mapping(source = "description", target = "description")
    @Mapping(source = "technologies", target="technologies")
    Capacity updateRequestToCapacity(UpdateCapacityRequest capacityRequest);

}
