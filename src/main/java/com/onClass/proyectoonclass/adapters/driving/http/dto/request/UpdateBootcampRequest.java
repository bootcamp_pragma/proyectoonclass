package com.onClass.proyectoonclass.adapters.driving.http.dto.request;

import com.onClass.proyectoonclass.domain.model.Capacity;
import com.onClass.proyectoonclass.domain.model.Version;
import jakarta.persistence.Column;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor
@Getter
public class UpdateBootcampRequest {
    private final Long idBootcamp;

    @NotNull
    private final String name;

    @NotNull
    private final String description;

    private final List<Capacity> capacities;

}
