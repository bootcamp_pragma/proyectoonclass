package com.onClass.proyectoonclass.adapters.driven.jpa.mysql.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name="Bootcamps")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BootcampEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idBootcamp;

    @Column(unique = true, nullable = false)
    private String name;

    @Column(nullable = false)
    private String description;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE})
    @JoinTable(
            name = "capacidad_bootcamp",
            joinColumns = @JoinColumn(name = "idBootcamp"),
            inverseJoinColumns = @JoinColumn(name = "idCapacity"),
            uniqueConstraints = { @UniqueConstraint(columnNames = {"idBootcamp", "idCapacity"})}
    )
    private List<CapacityEntity> capacities;

}
