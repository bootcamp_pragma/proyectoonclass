package com.onClass.proyectoonclass.adapters.driven.jpa.mysql.exception;

public class ElementNotFoundException extends RuntimeException{

    public ElementNotFoundException() {
        super();
    }
    public ElementNotFoundException(String message) {
        super(message);
    }
}
