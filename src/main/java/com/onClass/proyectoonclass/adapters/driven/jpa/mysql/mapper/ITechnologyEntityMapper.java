package com.onClass.proyectoonclass.adapters.driven.jpa.mysql.mapper;

import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.entity.TechnologyEntity;
import com.onClass.proyectoonclass.domain.model.Technology;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ITechnologyEntityMapper {

    @Mapping(source = "idTechnology", target ="idTechnology")
    @Mapping(source = "name", target="name")
    @Mapping(source = "description", target = "description")
    Technology toModel(TechnologyEntity technologyEntity);
    @Mapping(source = "idTechnology", target ="idTechnology")
    @Mapping(source = "name", target="name")
    @Mapping(source = "description", target = "description")
    TechnologyEntity toEntity(Technology technology);
    List<Technology> toModelList(List<TechnologyEntity> technologyEntities);
}
