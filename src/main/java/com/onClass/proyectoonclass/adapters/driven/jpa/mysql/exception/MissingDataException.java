package com.onClass.proyectoonclass.adapters.driven.jpa.mysql.exception;

public class MissingDataException extends RuntimeException{

    public MissingDataException() {
        super();
    }
    public MissingDataException(String message) {
        super(message);
    }
}
