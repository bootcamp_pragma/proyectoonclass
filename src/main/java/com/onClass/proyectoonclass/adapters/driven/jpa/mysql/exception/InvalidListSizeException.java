package com.onClass.proyectoonclass.adapters.driven.jpa.mysql.exception;

public class InvalidListSizeException  extends RuntimeException{

    public InvalidListSizeException() {
        super();
    }
    public InvalidListSizeException(String message) {
        super(message);
    }
}
