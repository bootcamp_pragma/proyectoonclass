package com.onClass.proyectoonclass.adapters.driven.jpa.mysql.mapper;

import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.entity.BootcampEntity;
import com.onClass.proyectoonclass.domain.model.Bootcamp;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface IBootcampEntityMapper {

    @Mapping(source = "idBootcamp", target="idBootcamp")
    @Mapping(source = "name", target="name")
    @Mapping(source = "description", target="description")
    @Mapping(source = "capacities", target="capacities")
    Bootcamp toModel(BootcampEntity bootcampEntity);

    @Mapping(source = "idBootcamp", target="idBootcamp")
    @Mapping(source = "name", target="name")
    @Mapping(source = "description", target="description")
    @Mapping(source = "capacities", target="capacities")
    BootcampEntity toEntity (Bootcamp bootcamp);

    List<Bootcamp> toModelList(List<BootcampEntity> bootcampEntities);
}
