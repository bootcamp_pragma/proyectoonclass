package com.onClass.proyectoonclass.adapters.driven.jpa.mysql.adapter;

import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.entity.BootcampEntity;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.entity.VersionEntity;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.exception.ElementNotFoundException;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.exception.NoDataFoundException;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.mapper.IBootcampEntityMapper;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.mapper.IVersionEntityMapper;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.repository.IBootcampRepository;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.repository.IVersionRepository;
import com.onClass.proyectoonclass.domain.model.Technology;
import com.onClass.proyectoonclass.domain.model.Version;
import com.onClass.proyectoonclass.domain.spi.IVersionPersistencePort;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class VersionAdapter implements IVersionPersistencePort {
    private final IVersionRepository versionRepository;
    private final IVersionEntityMapper versionEntityMapper;
    private final IBootcampRepository bootcampRepository;
    private final IBootcampEntityMapper bootcampEntityMapper;

    @Override
    public Version saveVersion(Version version) {
        BootcampEntity bootcamp = bootcampRepository.findById(version.getBootcamp().getIdBootcamp()).orElseThrow(ElementNotFoundException::new);
        version.setBootcamp(bootcampEntityMapper.toModel(bootcamp));
        return versionEntityMapper.toModel(versionRepository.save(versionEntityMapper.toEntity(version)));
    }

    @Override
    public List<Version> getAllVersionByBootcamp(Long idBootcamp, Integer page, Integer size, String sort) {
        BootcampEntity bootcamp = bootcampRepository.findById(idBootcamp).orElseThrow(ElementNotFoundException::new);
        Pageable pagination = PageRequest.of(page, size);
        if (!sort.isEmpty() && (sort.equalsIgnoreCase("ASC") || sort.equalsIgnoreCase("DESC") || sort.equalsIgnoreCase("DATEINI") || sort.equalsIgnoreCase("QUOTA")) ){
            Sort sorting;
            if (sort.equalsIgnoreCase("ASC")){
                sorting = Sort.by(Sort.Direction.ASC, "bootcamp.name");
            }
            else if (sort.equalsIgnoreCase("DESC")){
                sorting = Sort.by(Sort.Direction.DESC, "bootcamp.name");
            }
            else if (sort.equalsIgnoreCase("DATEINI")){
                sorting = Sort.by(Sort.Direction.ASC, "fechaInicio");
            }
            else {
                sorting = Sort.by(Sort.Direction.ASC, "cupo");
            }
            pagination = PageRequest.of(page, size, sorting);
        }
        List<VersionEntity> versions = versionRepository.findAllByBootcamp(bootcamp, pagination).getContent();
        return versionEntityMapper.toModelList(versions);
    }

    @Override
    public List<Version> getAllVersion(Integer page, Integer size, String sort) {
        Pageable pagination = PageRequest.of(page, size);
        if (sort!= null && !sort.isEmpty() && (sort.equalsIgnoreCase("DATEINI") || sort.equalsIgnoreCase("QUOTA")) ){
            Sort sorting;
            if (sort.equalsIgnoreCase("DATEINI")){
                sorting = Sort.by(Sort.Direction.ASC, "fechaInicio");
            }
            else {
                sorting = Sort.by(Sort.Direction.ASC, "cupo");
            }
            pagination = PageRequest.of(page, size, sorting);
        }

        List<VersionEntity> versions = versionRepository.findAll(pagination).getContent();
        return versionEntityMapper.toModelList(versions);
    }

    @Override
    public Version updateVersion(Version version) {
        BootcampEntity bootcamp = bootcampRepository.findById(version.getBootcamp().getIdBootcamp()).orElseThrow(ElementNotFoundException::new);
        version.setBootcamp(bootcampEntityMapper.toModel(bootcamp));
        return versionEntityMapper.toModel(versionRepository.save(versionEntityMapper.toEntity(version)));
    }
    @Override
    public void deleteVersion(Long idVersion) {
        if (versionRepository.findById(idVersion).isEmpty()) {
            throw new ElementNotFoundException();
        }
        versionRepository.deleteById(idVersion);
    }
}
