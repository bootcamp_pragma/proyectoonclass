package com.onClass.proyectoonclass.adapters.driven.jpa.mysql.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.*;

import java.util.List;


@Entity
@Table(name = "Tecnologias")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class TechnologyEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idTechnology;

    @Size(max = 50, message = "Nombre no debe exceder los 50 caracteres")
    @Column(unique = true, nullable = false)
    private String name;

    @Size(max = 90, message = "La descripción no debe superar los 90 caracteres")
    @Column(nullable = false)
    private String description;

    @ManyToMany(mappedBy = "technologies")
    private List<CapacityEntity> capacities;
}

