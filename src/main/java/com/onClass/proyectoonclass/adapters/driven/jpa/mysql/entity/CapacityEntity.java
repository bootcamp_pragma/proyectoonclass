package com.onClass.proyectoonclass.adapters.driven.jpa.mysql.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name="Capacidades")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CapacityEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idCapacity;

    @Column(unique = true, nullable = false)
    private String name;

    @Column(nullable = false)
    private String description;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE})
    @JoinTable(
            name = "capacidad_tecnologia",
            joinColumns = @JoinColumn(name = "idCapacity"),
            inverseJoinColumns = @JoinColumn(name = "idTechnology"),
            uniqueConstraints = { @UniqueConstraint(columnNames = {"idCapacity", "idTechnology"})}
    )
    private List<TechnologyEntity> technologies;

    @ManyToMany(mappedBy = "capacities")
    private List<BootcampEntity> bootcamp;

}
