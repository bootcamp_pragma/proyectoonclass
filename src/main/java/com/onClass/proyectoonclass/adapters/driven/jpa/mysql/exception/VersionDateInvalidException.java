package com.onClass.proyectoonclass.adapters.driven.jpa.mysql.exception;

public class VersionDateInvalidException extends RuntimeException{
    public VersionDateInvalidException(){
        super();
    }
    public VersionDateInvalidException(String message){
        super(message);
    }
}
