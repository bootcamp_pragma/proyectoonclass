package com.onClass.proyectoonclass.adapters.driven.jpa.mysql.adapter;

import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.entity.TechnologyEntity;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.exception.ElementNotFoundException;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.mapper.ITechnologyEntityMapper;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.repository.ITechnologyRepository;
import com.onClass.proyectoonclass.domain.model.Technology;
import com.onClass.proyectoonclass.domain.spi.ITechnologyPersistencePort;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;


@RequiredArgsConstructor
public class TechnologyAdapater implements ITechnologyPersistencePort {
    private final ITechnologyRepository technologyRepository;
    private final ITechnologyEntityMapper technologyEntityMapper;

    @Override
    public Technology saveTechnology(Technology technology) {
        return technologyEntityMapper.toModel(technologyRepository.save(technologyEntityMapper.toEntity(technology)));
    }
    @Override
    public Optional<Technology> getTechnology(String name) {
        TechnologyEntity technology = technologyRepository.findByName(name).orElse(null);
        return Optional.ofNullable(technologyEntityMapper.toModel(technology));
    }
    @Override
    public List<Technology> getAllTechnology(Integer page, Integer size, String sort) {
        Pageable pagination = PageRequest.of(page, size);
        if (!sort.isEmpty() && (sort.equalsIgnoreCase("ASC") || sort.equalsIgnoreCase("DESC")) ){
            Sort sorting;
            if (sort.equalsIgnoreCase("ASC")){
                sorting = Sort.by(Sort.Direction.ASC, "name");
            }
            else {
                sorting = Sort.by(Sort.Direction.DESC, "name");
            }
            pagination = PageRequest.of(page, size, sorting);
        }
        List<TechnologyEntity> technologies = technologyRepository.findAll(pagination).getContent();
        return technologyEntityMapper.toModelList(technologies);
    }

    @Override
    public Technology updateTechnology(Technology technology) {
        return technologyEntityMapper.toModel(technologyRepository.save(technologyEntityMapper.toEntity(technology)));
    }
    @Override
    public void deleteTechnology(Long idTechnology) {
        if (technologyRepository.findById(idTechnology).isEmpty()){
            throw new ElementNotFoundException();
        }
        technologyRepository.deleteById(idTechnology);
    }
}
