package com.onClass.proyectoonclass.adapters.driven.jpa.mysql.exception;

public class EntityAlreadyExistsException extends RuntimeException {

    public EntityAlreadyExistsException() {
        super();
    }
    public EntityAlreadyExistsException(String message) {
        super(message);
    }
}
