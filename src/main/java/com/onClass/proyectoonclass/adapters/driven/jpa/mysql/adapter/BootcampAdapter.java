package com.onClass.proyectoonclass.adapters.driven.jpa.mysql.adapter;

import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.entity.BootcampEntity;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.exception.*;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.mapper.IBootcampEntityMapper;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.repository.IBootcampRepository;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.repository.ICapacityRepository;
import com.onClass.proyectoonclass.configuration.Constans;
import com.onClass.proyectoonclass.domain.model.Bootcamp;
import com.onClass.proyectoonclass.domain.model.Capacity;
import com.onClass.proyectoonclass.domain.spi.IBootcampPersistencePort;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class BootcampAdapter implements IBootcampPersistencePort {
    private final IBootcampRepository bootcampRepository;
    private final IBootcampEntityMapper bootcampEntityMapper;
    private final ICapacityRepository capacityRepository;

    @Override
    public Bootcamp saveBootcamp(Bootcamp bootcamp) {
        if(bootcamp.getCapacities().isEmpty()){
            throw new MissingDataException(Constans.BOOTCAMP_CAPACITY_LIST_MISSING_DATA_EXCEPTION_MESSAGE);
        }
        if(bootcamp.getCapacities().size() < Constans.MINIMUM_SIZE_CAPACITY_LIST ||  bootcamp.getCapacities().size() > Constans.MAXIMUM_SIZE_CAPACITY_LIST){
            throw new InvalidListSizeException(Constans.BOOTCAMP_CAPACITY_INVALID_LIST_SIZE_EXCEPTION_MESSAGE);
        }
        else
        {
            for (Capacity capacity : bootcamp.getCapacities()){
                capacityRepository.findById(capacity.getIdCapacity()).orElseThrow(ElementNotFoundException::new);
            }
        }
        return bootcampEntityMapper.toModel(bootcampRepository.save(bootcampEntityMapper.toEntity(bootcamp)));
    }

    @Override
    public Optional<Bootcamp> getBootcamp(String name) {
        BootcampEntity bootcamp = bootcampRepository.findByName(name).orElse(null);
        return Optional.ofNullable(bootcampEntityMapper.toModel(bootcamp));
    }

    @Override
    public List<Bootcamp> getAllBootcamp(Integer page, Integer size, String sort) {
        Pageable pagination = PageRequest.of(page, size);
        if (!sort.isEmpty() && (sort.equalsIgnoreCase("ASC")||sort.equalsIgnoreCase("DESC"))){
            Sort sorting;
            if(sort.equalsIgnoreCase("ASC")){
                sorting = Sort.by(Sort.Direction.ASC,"name");
            }
            else{
                sorting = Sort.by(Sort.Direction.DESC, "name");
            }
            pagination = PageRequest.of(page, size, sorting);
        }
        List<BootcampEntity> bootCamps = bootcampRepository.findAll(pagination).getContent();
        return bootcampEntityMapper.toModelList(bootCamps);
    }

    @Override
    public Bootcamp updateBootcamp(Bootcamp bootcamp) {
        if(bootcamp.getCapacities().isEmpty()){
            throw new MissingDataException(Constans.BOOTCAMP_CAPACITY_LIST_MISSING_DATA_EXCEPTION_MESSAGE);
        }
        if(bootcamp.getCapacities().size() < Constans.MINIMUM_SIZE_CAPACITY_LIST ||  bootcamp.getCapacities().size() > Constans.MAXIMUM_SIZE_CAPACITY_LIST){
            throw new InvalidListSizeException(Constans.BOOTCAMP_CAPACITY_INVALID_LIST_SIZE_EXCEPTION_MESSAGE);
        }
        else{
            for (Capacity capacity : bootcamp.getCapacities()){
                capacityRepository.findById(capacity.getIdCapacity()).orElseThrow(ElementNotFoundException::new);
            }
        }
        return bootcampEntityMapper.toModel(bootcampRepository.save(bootcampEntityMapper.toEntity(bootcamp)));
    }

    @Override
    public void deleteBootcamp(Long idBootcamp) {
        if (bootcampRepository.findById(idBootcamp).isEmpty()){
            throw new ElementNotFoundException();
        }
        bootcampRepository.deleteById(idBootcamp);
    }
}
