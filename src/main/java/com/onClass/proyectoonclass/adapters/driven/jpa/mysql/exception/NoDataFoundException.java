package com.onClass.proyectoonclass.adapters.driven.jpa.mysql.exception;

public class NoDataFoundException extends RuntimeException {
    public NoDataFoundException(){
        super();
    }
    public NoDataFoundException(String message){
        super(message);
    }
}
