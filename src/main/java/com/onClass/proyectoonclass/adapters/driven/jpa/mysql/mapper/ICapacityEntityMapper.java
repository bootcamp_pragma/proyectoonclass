package com.onClass.proyectoonclass.adapters.driven.jpa.mysql.mapper;

import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.entity.CapacityEntity;
import com.onClass.proyectoonclass.domain.model.Capacity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ICapacityEntityMapper {
    @Mapping(source = "idCapacity", target="idCapacity")
    @Mapping(source = "name", target = "name")
    @Mapping(source = "description", target="description")
    @Mapping(source = "technologies", target="technologies")
    Capacity toModel(CapacityEntity capacityEntity);
    @Mapping(source = "idCapacity", target="idCapacity")
    @Mapping(source = "name", target = "name")
    @Mapping(source = "description", target="description")
    @Mapping(source = "technologies", target="technologies")
    CapacityEntity toEntity (Capacity capacity);

    List<Capacity> toModelList(List<CapacityEntity> capacityEntities);

}
