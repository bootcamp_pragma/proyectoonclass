package com.onClass.proyectoonclass.adapters.driven.jpa.mysql.adapter;

import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.entity.CapacityEntity;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.exception.*;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.mapper.ICapacityEntityMapper;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.repository.ICapacityRepository;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.repository.ITechnologyRepository;
import com.onClass.proyectoonclass.configuration.Constans;
import com.onClass.proyectoonclass.domain.model.Capacity;
import com.onClass.proyectoonclass.domain.model.Technology;
import com.onClass.proyectoonclass.domain.spi.ICapacityPersistencePort;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;


import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class CapacityAdapter implements ICapacityPersistencePort {
    private final ICapacityRepository capacityRepository;
    private final ICapacityEntityMapper capacityEntityMapper;
    private final ITechnologyRepository technologyRepository;

    @Override
    public Capacity saveCapacity(Capacity capacity) {
        if(capacity.getTechnologies().isEmpty()){
            throw new MissingDataException(Constans.CAPACITY_TECHNOLOGY_LIST_MISSING_DATA_EXCEPTION_MESSAGE);
        }
        if(capacity.getTechnologies().size() < Constans.MINIMUM_SIZE_TECHNOLOGY_LIST ||  capacity.getTechnologies().size() > Constans.MAXIMUM_SIZE_TECHNOLOGY_LIST){
            throw new InvalidListSizeException(Constans.CAPACITY_TECHNOLOGY_INVALID_LIST_SIZE_EXCEPTION_MESSAGE);
        }
        else
        {
            for (Technology technology : capacity.getTechnologies()){
                technologyRepository.findById(technology.getIdTechnology()).orElseThrow(ElementNotFoundException::new);
            }
        }
        return capacityEntityMapper.toModel(capacityRepository.save(capacityEntityMapper.toEntity(capacity)));
    }

    @Override
    public Optional<Capacity> getCapacity(String name) {
        CapacityEntity capacity = capacityRepository.findByName(name).orElse(null);
        return Optional.ofNullable(capacityEntityMapper.toModel(capacity));
    }

    @Override
    public List<Capacity> getAllCapacity(Integer page, Integer size, String sort) {
        Pageable pagination = PageRequest.of(page, size);
        if (!sort.isEmpty() && (sort.equalsIgnoreCase("ASC") || sort.equalsIgnoreCase("DESC"))){
            Sort sorting;
            if (sort.equalsIgnoreCase("ASC")){
                sorting = Sort.by(Sort.Direction.ASC,"name");
            }
            else {
                sorting = Sort.by(Sort.Direction.DESC, "name");
            }
            pagination = PageRequest.of(page, size, sorting);
        }
        List<CapacityEntity> capacities = capacityRepository.findAll(pagination).getContent();
        return capacityEntityMapper.toModelList(capacities);

    }

    @Override
    public Capacity updateCapacity(Capacity capacity) {
        if(capacity.getTechnologies().isEmpty()){
            throw new MissingDataException(Constans.CAPACITY_TECHNOLOGY_LIST_MISSING_DATA_EXCEPTION_MESSAGE);
        }
        if(capacity.getTechnologies().size() < Constans.MINIMUM_SIZE_TECHNOLOGY_LIST || capacity.getTechnologies().size() > Constans.MAXIMUM_SIZE_TECHNOLOGY_LIST){
            throw new InvalidListSizeException(Constans.CAPACITY_TECHNOLOGY_INVALID_LIST_SIZE_EXCEPTION_MESSAGE);
        }
        else{
            for (Technology technology : capacity.getTechnologies()){
                technologyRepository.findById(technology.getIdTechnology()).orElseThrow(ElementNotFoundException::new);
            }
        }
        return capacityEntityMapper.toModel(capacityRepository.save(capacityEntityMapper.toEntity(capacity)));
    }

    @Override
    public void deleteCapacity(Long idCapacity) {
        if (capacityRepository.findById(idCapacity).isEmpty()){
            throw new ElementNotFoundException();
        }
        capacityRepository.deleteById(idCapacity);
    }
}
