package com.onClass.proyectoonclass.adapters.driven.jpa.mysql.repository;

import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.entity.BootcampEntity;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.entity.VersionEntity;
import com.onClass.proyectoonclass.domain.model.Version;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface IVersionRepository extends JpaRepository<VersionEntity, Long> {

    Page<VersionEntity> findAllByBootcamp(BootcampEntity Bootcamp, Pageable pageable);

    Page<VersionEntity> findAll(Pageable pageable);
}
