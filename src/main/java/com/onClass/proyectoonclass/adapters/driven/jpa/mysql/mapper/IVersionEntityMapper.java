package com.onClass.proyectoonclass.adapters.driven.jpa.mysql.mapper;

import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.entity.VersionEntity;
import com.onClass.proyectoonclass.domain.model.Version;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface IVersionEntityMapper {
    @Mapping(source = "idVersion", target="idVersion")
    @Mapping(source = "cupo", target = "cupo")
    @Mapping(source = "fechaInicio", target="fechaInicio")
    @Mapping(source = "fechaFin", target="fechaFin")
    @Mapping(source = "bootcamp.idBootcamp", target="bootcamp.idBootcamp")
    @Mapping(source = "bootcamp.name", target="bootcamp.name")
    @Mapping(source = "bootcamp.description", target="bootcamp.description")
    Version toModel(VersionEntity versionEntity);
    @Mapping(source = "idVersion", target="idVersion")
    @Mapping(source = "cupo", target = "cupo")
    @Mapping(source = "fechaInicio", target="fechaInicio")
    @Mapping(source = "fechaFin", target="fechaFin")
    @Mapping(source = "bootcamp.idBootcamp", target="bootcamp.idBootcamp")
    @Mapping(source = "bootcamp.name", target="bootcamp.name")
    @Mapping(source = "bootcamp.description", target="bootcamp.description")
    VersionEntity toEntity(Version version);

    List<Version> toModelList(List<VersionEntity> versionEntities);

}
