package com.onClass.proyectoonclass.adapters.driven.jpa.mysql.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Entity
@Table(name="Version", uniqueConstraints = {@UniqueConstraint(name = "UniqueVersionByBootcamp",columnNames = {"cupo", "fechaInicio", "fechaFin", "bootcamp" })})
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class VersionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idVersion;
    @Column(nullable = false)
    private Integer cupo;
    @Column(nullable = false)
    private Date fechaInicio;
    @Column(nullable = false)
    private Date fechaFin;

    @ManyToOne
    @JoinColumn(name = "idBootcamp")
    private BootcampEntity bootcamp;

}
