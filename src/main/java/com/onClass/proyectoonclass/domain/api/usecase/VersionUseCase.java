package com.onClass.proyectoonclass.domain.api.usecase;

import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.exception.NoDataFoundException;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.exception.VersionDateInvalidException;
import com.onClass.proyectoonclass.configuration.Constans;
import com.onClass.proyectoonclass.domain.api.IVersionServicePort;
import com.onClass.proyectoonclass.domain.model.Bootcamp;
import com.onClass.proyectoonclass.domain.model.Version;
import com.onClass.proyectoonclass.domain.spi.IVersionPersistencePort;

import java.util.List;

public class VersionUseCase implements IVersionServicePort {

    private final IVersionPersistencePort versionPersistencePort;

    public VersionUseCase(IVersionPersistencePort versionPersistencePort) {
        this.versionPersistencePort = versionPersistencePort;
    }

    @Override
    public Version saveVersion(Version version) {
        if(version.getFechaFin().before(version.getFechaInicio())){
            throw new VersionDateInvalidException(Constans.VERSION_BOOTCAMP_DATE_INVALID_EXCEPTION_MESSAGE);
        }
        for (Version versionDB : versionPersistencePort.getAllVersion(0, Integer.MAX_VALUE, null)) {
            if(versionDB.getCupo().equals(version.getCupo())
                    && versionDB.getFechaInicio().getTime() == version.getFechaInicio().getTime()
                    && versionDB.getFechaFin().getTime() == version.getFechaFin().getTime()
                    && versionDB.getBootcamp().getIdBootcamp().equals(version.getBootcamp().getIdBootcamp())) {
                throw new VersionDateInvalidException(Constans.VERSION_BOOTCAMP_ALREADY_EXISTS_EXCEPTION_MESSAGE);
            }
        }
        return versionPersistencePort.saveVersion(version);
    }

    @Override
    public List<Version> getAllVersionByBootcamp(Long idBootcamp, Integer page, Integer size, String sort) {
        if(versionPersistencePort.getAllVersionByBootcamp(idBootcamp, page, size, sort).isEmpty()){
            throw new NoDataFoundException(Constans.NO_DATA_FOUND_EXCEPTION_MESSAGE);
        }

        return versionPersistencePort.getAllVersionByBootcamp(idBootcamp,page, size, sort);
    }

    @Override
    public List<Version> getAllVersion(Integer page, Integer size, String sort) {
        if(versionPersistencePort.getAllVersion(page, size, sort).isEmpty()){
            throw new NoDataFoundException(Constans.NO_DATA_FOUND_EXCEPTION_MESSAGE);
        }
        return versionPersistencePort.getAllVersion(page, size, sort);
    }

    @Override
    public Version updateVersion(Version version) {
        if(version.getFechaFin().before(version.getFechaInicio())){
            throw new VersionDateInvalidException(Constans.VERSION_BOOTCAMP_DATE_INVALID_EXCEPTION_MESSAGE);
        }
        return versionPersistencePort.updateVersion(version);
    }

    @Override
    public void deleteVersion(Long idVersion) {
        versionPersistencePort.deleteVersion(idVersion);
    }

}
