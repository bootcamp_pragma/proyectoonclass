package com.onClass.proyectoonclass.domain.api.usecase;

import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.exception.ElementNotFoundException;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.exception.EntityAlreadyExistsException;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.exception.NoDataFoundException;
import com.onClass.proyectoonclass.configuration.Constans;
import com.onClass.proyectoonclass.domain.api.ITechnologyServicePort;
import com.onClass.proyectoonclass.domain.model.Technology;
import com.onClass.proyectoonclass.domain.spi.ITechnologyPersistencePort;

import java.util.List;
import java.util.Optional;

public class TechnologyUseCase implements ITechnologyServicePort {

    private final ITechnologyPersistencePort technologyPersistencePort;

    public TechnologyUseCase(ITechnologyPersistencePort technologyPersistencePort) {
        this.technologyPersistencePort=technologyPersistencePort;
    }

    @Override
    public Technology saveTechnology(Technology technology) {
        if(technologyPersistencePort.getTechnology(technology.getName()).isPresent()) {
            throw new EntityAlreadyExistsException(Constans.TECHNOLOGY_ALREADY_EXISTS_EXCEPTION_MESSAGE);
        }
        return technologyPersistencePort.saveTechnology(technology);
    }

    @Override
    public Optional<Technology> getTechnology(String name) {
        if(technologyPersistencePort.getTechnology(name).isEmpty()) {
            throw new NoDataFoundException(Constans.NO_DATA_FOUND_EXCEPTION_MESSAGE);
        }
       return technologyPersistencePort.getTechnology(name);
    }

    @Override
    public List<Technology> getAllTechnology(Integer page, Integer size, String sort) {
        if(technologyPersistencePort.getAllTechnology(page, size, sort).isEmpty()) {
            throw new NoDataFoundException(Constans.NO_DATA_FOUND_EXCEPTION_MESSAGE);
        }
        return technologyPersistencePort.getAllTechnology(page, size, sort);
    }

    @Override
    public Technology updateTechnology(Technology technology) {
        Technology technologyBD = technologyPersistencePort.getTechnology(technology.getName()).orElse(null);

        if(technologyBD == null || !technologyBD.getIdTechnology().equals(technology.getIdTechnology())) {
            throw new ElementNotFoundException(Constans.ELEMENT_NOT_FOUND_EXCEPTION_MESSAGE);
        }
        return technologyPersistencePort.updateTechnology(technology);
    }

    @Override
    public void deleteTechnology(Long id) {
        technologyPersistencePort.deleteTechnology(id);
    }
}
