package com.onClass.proyectoonclass.domain.api.usecase;

import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.exception.*;
import com.onClass.proyectoonclass.configuration.Constans;
import com.onClass.proyectoonclass.domain.api.IBootcampServicePort;
import com.onClass.proyectoonclass.domain.model.Bootcamp;
import com.onClass.proyectoonclass.domain.model.Capacity;
import com.onClass.proyectoonclass.domain.spi.IBootcampPersistencePort;
import com.onClass.proyectoonclass.domain.spi.ICapacityPersistencePort;

import java.util.List;
import java.util.Optional;

public class BootcampUseCase implements IBootcampServicePort {

    private final IBootcampPersistencePort bootcampPersistencePort;

    public BootcampUseCase(IBootcampPersistencePort bootcampPersistencePort) {
        this.bootcampPersistencePort = bootcampPersistencePort;
    }

    @Override
    public Bootcamp saveBootcamp(Bootcamp bootcamp) {
        if(bootcampPersistencePort.getBootcamp(bootcamp.getName()).isPresent()){
            throw new EntityAlreadyExistsException(Constans.BOOTCAMP_ALREADY_EXISTS_EXCEPTION_MESSAGE);
        }
        return bootcampPersistencePort.saveBootcamp(bootcamp);
    }

    @Override
    public Optional<Bootcamp> getBootcamp(String name) {
        if (bootcampPersistencePort.getBootcamp(name).isEmpty()) {
            throw new NoDataFoundException(Constans.NO_DATA_FOUND_EXCEPTION_MESSAGE);
        }
        return bootcampPersistencePort.getBootcamp(name);
    }

    @Override
    public List<Bootcamp> getAllBootcamp(Integer page, Integer size, String sort) {
        List<Bootcamp> bootCamps = bootcampPersistencePort.getAllBootcamp(page, size, sort);
        if(bootCamps.isEmpty()){
            throw new NoDataFoundException(Constans.NO_DATA_FOUND_EXCEPTION_MESSAGE);
        }
        return bootCamps;
    }

    @Override
    public Bootcamp updateBootcamp(Bootcamp bootcamp) {
        if(bootcampPersistencePort.getBootcamp(bootcamp.getName()).isEmpty()){
            throw new NoDataFoundException(Constans.NO_DATA_FOUND_EXCEPTION_MESSAGE);
        }
        return bootcampPersistencePort.updateBootcamp(bootcamp);
    }

    @Override
    public void deleteBootcamp(Long idBootcamp) {
        bootcampPersistencePort.deleteBootcamp(idBootcamp);
    }
}
