package com.onClass.proyectoonclass.domain.api.usecase;

import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.exception.*;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.repository.ITechnologyRepository;
import com.onClass.proyectoonclass.configuration.Constans;
import com.onClass.proyectoonclass.domain.api.ICapacityServicePort;
import com.onClass.proyectoonclass.domain.model.Capacity;
import com.onClass.proyectoonclass.domain.model.Technology;
import com.onClass.proyectoonclass.domain.spi.ICapacityPersistencePort;
import com.onClass.proyectoonclass.domain.spi.ITechnologyPersistencePort;

import java.util.List;
import java.util.Optional;

public class CapacityUseCase implements ICapacityServicePort {

    private final ICapacityPersistencePort capacityPersistencePort;

    public CapacityUseCase(ICapacityPersistencePort capacityPersistencePort) {
        this.capacityPersistencePort = capacityPersistencePort;
    }

    @Override
    public Capacity saveCapacity(Capacity capacity) {
        if(capacityPersistencePort.getCapacity(capacity.getName()).isPresent()){
            throw new EntityAlreadyExistsException(Constans.CAPACITY_ALREADY_EXISTS_EXCEPTION_MESSAGE);
        }
        return capacityPersistencePort.saveCapacity(capacity);
    }

    @Override
    public Optional<Capacity> getCapacity(String name) {
        if(capacityPersistencePort.getCapacity(name).isEmpty()) {
            throw new NoDataFoundException(Constans.NO_DATA_FOUND_EXCEPTION_MESSAGE);
        }
        return capacityPersistencePort.getCapacity(name);
    }

    @Override
    public List<Capacity> getAllCapacity(Integer page, Integer size, String sort) {
        if(capacityPersistencePort.getAllCapacity(page, size, sort).isEmpty()) {
        throw new NoDataFoundException(Constans.NO_DATA_FOUND_EXCEPTION_MESSAGE);
    }
        return capacityPersistencePort.getAllCapacity(page, size, sort);
    }

    @Override
    public Capacity updateCapacity(Capacity capacity) {
        Capacity capacityBD = capacityPersistencePort.getCapacity(capacity.getName()).orElse(null);

        if(capacityBD == null || !capacityBD.getIdCapacity().equals(capacity.getIdCapacity())) {
            throw new ElementNotFoundException(Constans.ELEMENT_NOT_FOUND_EXCEPTION_MESSAGE);
        }
        if(capacityPersistencePort.getCapacity(capacity.getName()).isEmpty()){
            throw new NoDataFoundException(Constans.NO_DATA_FOUND_EXCEPTION_MESSAGE);
        }

        return capacityPersistencePort.updateCapacity(capacity);
    }

    @Override
    public void deleteCapacity(Long idCapacity) {
        capacityPersistencePort.deleteCapacity(idCapacity);
    }
}
