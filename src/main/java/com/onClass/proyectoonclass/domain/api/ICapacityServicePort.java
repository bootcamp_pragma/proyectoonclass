package com.onClass.proyectoonclass.domain.api;

import com.onClass.proyectoonclass.domain.model.Capacity;

import java.util.List;
import java.util.Optional;

public interface ICapacityServicePort {
    Capacity saveCapacity(Capacity capacity);
    Optional<Capacity> getCapacity(String name);
    List<Capacity> getAllCapacity(Integer page, Integer size, String Sort);
    Capacity updateCapacity(Capacity capacity);
    void deleteCapacity(Long idCapacity);
}
