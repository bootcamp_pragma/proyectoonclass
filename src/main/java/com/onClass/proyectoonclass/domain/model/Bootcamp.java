package com.onClass.proyectoonclass.domain.model;

import com.onClass.proyectoonclass.domain.exception.EmptyFieldException;
import com.onClass.proyectoonclass.domain.util.DomainConstants;

import java.util.List;

import static java.util.Objects.requireNonNull;

public class Bootcamp {
    private final Long idBootcamp;

    private final String name;

    private final String description;

    private final List<Capacity> capacities;

    public Bootcamp(Long idBootcamp, String name, String description, List<Capacity> capacities) {
        if(name.trim().isEmpty()){
            throw new EmptyFieldException(DomainConstants.Field.NAME.toString());
        }
        if(description.trim().isEmpty()){
            throw new EmptyFieldException(DomainConstants.Field.DESCRIPTION.toString());
        }
        this.idBootcamp = idBootcamp;
        this.name =requireNonNull(name, DomainConstants.FIELD_NAME_NULL_MESSAGE);
        this.description = requireNonNull(description, DomainConstants.FIELD_DESCRIPTION_NULL_MESSAGE);
        this.capacities = capacities;
    }

    public Long getIdBootcamp() {
        return idBootcamp;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public List<Capacity> getCapacities() {
        return capacities;
    }
}
