package com.onClass.proyectoonclass.domain.model;

import com.onClass.proyectoonclass.domain.exception.EmptyFieldException;
import com.onClass.proyectoonclass.domain.util.DomainConstants;


import static java.util.Objects.requireNonNull;

public class Technology {

    private final Long idTechnology;

    private final String name;

    private final String description;


    public Technology(Long idTechnology, String name, String description) {
        if (name.trim().isEmpty()) {
            throw new EmptyFieldException(DomainConstants.Field.NAME.toString());
        }
        if (description.trim().isEmpty()) {
            throw new EmptyFieldException(DomainConstants.Field.DESCRIPTION.toString());
        }
        this.idTechnology = idTechnology;
        this.name = requireNonNull(name, DomainConstants.FIELD_NAME_NULL_MESSAGE);
        this.description = requireNonNull(description, DomainConstants.FIELD_DESCRIPTION_NULL_MESSAGE);
    }
    public Long getIdTechnology() {
        return idTechnology;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
