package com.onClass.proyectoonclass.domain.model;

import com.onClass.proyectoonclass.domain.exception.EmptyFieldException;
import com.onClass.proyectoonclass.domain.util.DomainConstants;

import java.util.List;

import static java.util.Objects.requireNonNull;

public class Capacity {

    private final Long idCapacity;

    private final String name;

    private final String description;
    private final List<Technology> technologies;

    public Capacity(Long idCapacity, String name, String description, List<Technology> technologies) {
        if (name.trim().isEmpty()){
            throw new EmptyFieldException(DomainConstants.Field.NAME.toString());
        }
        if (description.trim().isEmpty()){
            throw new EmptyFieldException(DomainConstants.Field.DESCRIPTION.toString());
        }
        this.idCapacity = idCapacity;
        this.name = requireNonNull (name, DomainConstants.FIELD_NAME_NULL_MESSAGE);
        this.description = requireNonNull(description, DomainConstants.FIELD_DESCRIPTION_NULL_MESSAGE) ;
        this.technologies = technologies;
    }

    public Long getIdCapacity() {
        return idCapacity;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public List<Technology> getTechnologies() {return technologies;
    }
}
