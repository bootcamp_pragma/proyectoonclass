package com.onClass.proyectoonclass.domain.model;

import java.util.Date;

public class Version {
    private final Long idVersion;
    private final Integer cupo;
    private final Date fechaInicio;
    private final Date fechaFin;
    private Bootcamp bootcamp;
    public Version(Long idVersion, Integer cupo, Date fechaInicio, Date fechaFin, Bootcamp bootcamp) {
        this.idVersion = idVersion;
        this.cupo = cupo;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.bootcamp = bootcamp;
    }

    public Long getIdVersion() {
        return idVersion;
    }

    public Integer getCupo() {
        return cupo;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public Bootcamp getBootcamp() { return bootcamp; }
    public void setBootcamp(Bootcamp bootcamp) { this.bootcamp = bootcamp; }
}
