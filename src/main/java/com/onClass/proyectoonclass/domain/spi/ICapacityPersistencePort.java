package com.onClass.proyectoonclass.domain.spi;

import com.onClass.proyectoonclass.domain.model.Capacity;

import java.util.List;
import java.util.Optional;

public interface ICapacityPersistencePort {
    Capacity saveCapacity (Capacity capacity);
    Optional<Capacity> getCapacity (String name);
    List<Capacity> getAllCapacity (Integer page, Integer size, String sort);
    Capacity updateCapacity (Capacity capacity);
    void deleteCapacity(Long idCapacity);
}
