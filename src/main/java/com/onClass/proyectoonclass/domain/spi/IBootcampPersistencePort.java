package com.onClass.proyectoonclass.domain.spi;

import com.onClass.proyectoonclass.domain.model.Bootcamp;

import java.util.List;
import java.util.Optional;

public interface IBootcampPersistencePort {
    Bootcamp saveBootcamp(Bootcamp bootcamp);
    Optional<Bootcamp> getBootcamp(String name);
    List<Bootcamp> getAllBootcamp(Integer page, Integer size, String sort);
    Bootcamp updateBootcamp(Bootcamp bootcamp);

    void deleteBootcamp(Long idBootcamp);
}
