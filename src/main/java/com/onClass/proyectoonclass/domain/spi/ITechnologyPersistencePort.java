package com.onClass.proyectoonclass.domain.spi;

import com.onClass.proyectoonclass.domain.model.Technology;

import java.util.List;
import java.util.Optional;

public interface ITechnologyPersistencePort {

    Technology saveTechnology (Technology technology);
    Optional<Technology> getTechnology (String name);
    List<Technology> getAllTechnology (Integer page, Integer size, String sort);
    Technology updateTechnology (Technology technology);
    void deleteTechnology(Long id);
}
