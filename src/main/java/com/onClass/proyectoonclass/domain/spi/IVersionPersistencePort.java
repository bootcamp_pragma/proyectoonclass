package com.onClass.proyectoonclass.domain.spi;

import com.onClass.proyectoonclass.domain.model.Bootcamp;
import com.onClass.proyectoonclass.domain.model.Version;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface IVersionPersistencePort {
    Version saveVersion(Version version);
    List<Version> getAllVersionByBootcamp(Long idBootcamp, Integer page, Integer size, String sort);
    List<Version> getAllVersion(Integer page, Integer size, String sort);

    Version updateVersion(Version version);
    void deleteVersion(Long idVersion);
}
