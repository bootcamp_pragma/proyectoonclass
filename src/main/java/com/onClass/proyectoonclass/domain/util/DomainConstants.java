package com.onClass.proyectoonclass.domain.util;

public class DomainConstants {

    private DomainConstants () { throw new IllegalStateException("Utility class"); }

    public enum Field {
        NAME,
        DESCRIPTION,
    }

    public static final String FIELD_NAME_NULL_MESSAGE = "El campo 'nombre' no debe ir vacío";

    public static final String FIELD_DESCRIPTION_NULL_MESSAGE = "El campo 'descripción' no debe ir vacío";

}
