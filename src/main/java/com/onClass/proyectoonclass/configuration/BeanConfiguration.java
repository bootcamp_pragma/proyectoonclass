package com.onClass.proyectoonclass.configuration;


import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.adapter.BootcampAdapter;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.adapter.CapacityAdapter;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.adapter.TechnologyAdapater;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.adapter.VersionAdapter;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.mapper.IBootcampEntityMapper;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.mapper.ICapacityEntityMapper;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.mapper.ITechnologyEntityMapper;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.mapper.IVersionEntityMapper;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.repository.IBootcampRepository;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.repository.ICapacityRepository;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.repository.ITechnologyRepository;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.repository.IVersionRepository;
import com.onClass.proyectoonclass.domain.api.IBootcampServicePort;
import com.onClass.proyectoonclass.domain.api.ICapacityServicePort;
import com.onClass.proyectoonclass.domain.api.ITechnologyServicePort;
import com.onClass.proyectoonclass.domain.api.IVersionServicePort;
import com.onClass.proyectoonclass.domain.api.usecase.BootcampUseCase;
import com.onClass.proyectoonclass.domain.api.usecase.CapacityUseCase;
import com.onClass.proyectoonclass.domain.api.usecase.TechnologyUseCase;
import com.onClass.proyectoonclass.domain.api.usecase.VersionUseCase;
import com.onClass.proyectoonclass.domain.spi.IBootcampPersistencePort;
import com.onClass.proyectoonclass.domain.spi.ICapacityPersistencePort;
import com.onClass.proyectoonclass.domain.spi.ITechnologyPersistencePort;
import com.onClass.proyectoonclass.domain.spi.IVersionPersistencePort;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class BeanConfiguration {

    private final ITechnologyRepository technologyRepository;
    private final ITechnologyEntityMapper technologyEntityMapper;

    private final ICapacityRepository capacityRepository;
    private final ICapacityEntityMapper capacityEntityMapper;

    private final IBootcampRepository bootcampRepository;
    private final IBootcampEntityMapper bootcampEntityMapper;

    private final IVersionRepository versionRepository;
    private final IVersionEntityMapper versionEntityMapper;
    @Bean
    public ITechnologyPersistencePort technologyPersistencePort(){
        return new TechnologyAdapater(technologyRepository, technologyEntityMapper);
    }
    @Bean
    public ITechnologyServicePort technologyServicePort(){
        return new TechnologyUseCase(technologyPersistencePort());
    }
    @Bean
    public ICapacityPersistencePort capacityPersistencePort(){
        return new CapacityAdapter(capacityRepository, capacityEntityMapper, technologyRepository);
    }
    @Bean
    public ICapacityServicePort capacityServicePort() {
        return new CapacityUseCase(capacityPersistencePort()); }

    @Bean
    public IBootcampPersistencePort bootcampPersistencePort() {
        return new BootcampAdapter(bootcampRepository, bootcampEntityMapper, capacityRepository);
    }
    @Bean
    public IBootcampServicePort bootcampServicePort() {
        return new BootcampUseCase(bootcampPersistencePort()); }

    @Bean
    public IVersionPersistencePort versionPersistencePort() {
        return new VersionAdapter(versionRepository, versionEntityMapper, bootcampRepository, bootcampEntityMapper);}

    @Bean
    public IVersionServicePort versionServicePort() {
        return new VersionUseCase(versionPersistencePort());
    }
}
