package com.onClass.proyectoonclass.configuration;

public class Constans {

    private Constans(){throw new IllegalStateException("utility class");}

    public static final int MINIMUM_SIZE_TECHNOLOGY_LIST = 3;
    public static final int MAXIMUM_SIZE_TECHNOLOGY_LIST = 20;
    public static final int MINIMUM_SIZE_CAPACITY_LIST = 1;
    public static final int MAXIMUM_SIZE_CAPACITY_LIST = 4;
    public static final String NO_DATA_FOUND_EXCEPTION_MESSAGE = "No data was found in the database";
    public static final String ELEMENT_NOT_FOUND_EXCEPTION_MESSAGE = "The element indicated does not exist";
    public static final String TECHNOLOGY_ALREADY_EXISTS_EXCEPTION_MESSAGE = "The technology you want to create already exists";
    public static final String EMPTY_FIELD_EXCEPTION_MESSAGE = "Field %s can not be empty";
    public static final String CAPACITY_ALREADY_EXISTS_EXCEPTION_MESSAGE = "The capacity you want to create already exists";
    public static final String CAPACITY_TECHNOLOGY_LIST_MISSING_DATA_EXCEPTION_MESSAGE = "The technologies associated with its capacity were not found.";
    public static final String CAPACITY_TECHNOLOGY_INVALID_LIST_SIZE_EXCEPTION_MESSAGE = "The technologies associated with its capacity do not meet the minimum (" + MINIMUM_SIZE_TECHNOLOGY_LIST + ") or maximum (" + MAXIMUM_SIZE_TECHNOLOGY_LIST + ") quantity.";
    public static final String BOOTCAMP_ALREADY_EXISTS_EXCEPTION_MESSAGE = "The bootcamp you want to create already exists";
    public static final String BOOTCAMP_CAPACITY_LIST_MISSING_DATA_EXCEPTION_MESSAGE = "The capacities associated with its bootcamp were not found.";
    public static final String BOOTCAMP_CAPACITY_INVALID_LIST_SIZE_EXCEPTION_MESSAGE = "The capacities associated with its bootcamp do not meet the minimum (" + MINIMUM_SIZE_CAPACITY_LIST + ") or maximum (" + MAXIMUM_SIZE_CAPACITY_LIST + ") quantity.";
    public static final String VERSION_BOOTCAMP_DATE_INVALID_EXCEPTION_MESSAGE = "The end date of the bootcamp cannot be greater than the end date.";
    public static final String VERSION_BOOTCAMP_ALREADY_EXISTS_EXCEPTION_MESSAGE = "The version of the bootcamp you want to create already exists";

}

