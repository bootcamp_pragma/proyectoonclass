package com.onClass.proyectoonclass;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProyectoOnClassApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProyectoOnClassApplication.class, args);
	}

}
