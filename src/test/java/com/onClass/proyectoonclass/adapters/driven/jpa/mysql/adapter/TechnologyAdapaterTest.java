package com.onClass.proyectoonclass.adapters.driven.jpa.mysql.adapter;

import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.entity.TechnologyEntity;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.mapper.ITechnologyEntityMapper;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.mapper.ITechnologyEntityMapperImpl;
import com.onClass.proyectoonclass.adapters.driven.jpa.mysql.repository.ITechnologyRepository;
import com.onClass.proyectoonclass.domain.model.Technology;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TechnologyAdapaterTest {

    @Mock
    private  ITechnologyRepository technologyRepository;
    @Mock
    private  ITechnologyEntityMapper technologyEntityMapper;
    @InjectMocks
    private TechnologyAdapater technologyAdapater;


    @Test
    public void testSaveTechnology() {
        Technology technology = new Technology(0L, "Java", "Lenguaje de programación");
        TechnologyEntity technologyEntity = new TechnologyEntity(0L, "Java", "Lenguaje de programación", null);

        when(technologyEntityMapper.toEntity(technology)).thenReturn(technologyEntity);
        when(technologyRepository.save(technologyEntity)).thenReturn(technologyEntity);
        when(technologyEntityMapper.toModel(technologyEntity)).thenReturn(technology);

        Technology savedTechnology = technologyAdapater.saveTechnology(technology);

        assertNotNull(savedTechnology);
        // Agregar más aserciones según sea necesario
    }

    @Test
    public void testGetTechnology() {
        String name = "Java"; // Nombre de la tecnología a buscar
        TechnologyEntity technologyEntity = new TechnologyEntity(/* agregar valores de prueba */);
        Technology technology = new Technology(/* agregar valores de prueba */);

        when(technologyRepository.findByName(name)).thenReturn(Optional.of(technologyEntity));
        when(technologyEntityMapper.toModel(technologyEntity)).thenReturn(technology);

        Optional<Technology> retrievedTechnology = technologyAdapater.getTechnology(name);

        assertTrue(retrievedTechnology.isPresent());
        assertEquals(technology, retrievedTechnology.get());
    }

    @Test
    public void testGetAllTechnology() {
        // Configurar mocks y valores de prueba
        List<TechnologyEntity> technologyEntities = new ArrayList<>();
        // Agregar elementos a la lista de entidades de tecnología

        when(technologyRepository.findAll(any())).thenReturn(new PageImpl<>(technologyEntities));
        // Configurar el comportamiento del mapper

        List<Technology> technologies = technologyAdapater.getAllTechnology(0, 10, "ASC");

        assertNotNull(technologies);
        // Realizar aserciones según sea necesario
    }

    @Test
    public void testUpdateTechnology() {
        Technology technology = new Technology(/* agregar valores de prueba */);
        TechnologyEntity technologyEntity = new TechnologyEntity(/* agregar valores de prueba */);

        when(technologyEntityMapper.toEntity(technology)).thenReturn(technologyEntity);
        when(technologyRepository.save(technologyEntity)).thenReturn(technologyEntity);
        when(technologyEntityMapper.toModel(technologyEntity)).thenReturn(technology);

        Technology updatedTechnology = technologyAdapter.updateTechnology(technology);

        assertNotNull(updatedTechnology);

    }

    @Test
    public void testDeleteTechnology() {
        Long idTechnology = 1L; // ID de la tecnología a eliminar

        // Simular que la tecnología existe en el repositorio
        when(technologyRepository.findById(idTechnology)).thenReturn(Optional.of(new TechnologyEntity()));

        assertDoesNotThrow(() -> technologyAdapter.deleteTechnology(idTechnology));
        // Verificar que el método deleteById del repositorio haya sido llamado con el ID correcto
        verify(technologyRepository).deleteById(idTechnology);// Configurar mocks y valores de prueba para el método de eliminación
    }
}